from vvpd14 import input_check, task_solution

def test_one():
    """Проверяем функцию проверки ввода при подаче строки 'Пять'."""
    assert input_check("Пять") == False

def test_two():
    """Пытаемся 'обмануть' ф-цию проверки ввода"""
    assert input_check('5') == False

def test_three():
    """Подаём нормальное значение"""
    assert input_check(5) == True

def test_task_solution_one():
    """Проверяем функцию решения задачи"""
    assert task_solution(4) == []
    
def test_task_solution_two():
    """Проверяем функцию решения задачи"""
    assert task_solution(7) == [1, 3]